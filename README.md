# ARTOFDUO UI

## Development

For building the application, you need to have [NodeJs](https://nodejs.org/en/) with npm. You also need to have [Bower](http://bower.io/) installed globally. 

After [downloading](#download) run the following commands from the project folder:

Install bower globally
```
npm install -g bower
```

Install npm dependencies 
```
npm install
```

Install front-end bower dependencies 
```
bower install
```

Build the project and start local web server
```
npm start
```

Open the project [http://localhost:4000](http://localhost:4000).

> The project is built by Gulp. You can read more info in [Build Tasks](#build-tasks) section